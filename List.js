import { Save, Load } from './Data.js'

var saveName = document.getElementsByName("SaveName")[0].content;
var input = document.querySelector('input');
var count = document.querySelector('p1');
var listItems = document.querySelectorAll('dt');
input.addEventListener('input', filter);

var listItemsInput = document.querySelectorAll('input[type="checkbox"]');
var temp = "Count: "
count.innerHTML = 'Count: 0/' + (listItemsInput.length);

listItemsInput.forEach(function(x)
{
    x.addEventListener('input', countItems);
    x.addEventListener('input', SaveData);
})

LoadData()

function SaveData()
{
    console.log("Saving...");
    let data = "";
    listItemsInput.forEach(function(x)
    {
        data += x.checked + "|";
    })
    Save(saveName, data);
}

function LoadData()
{
    console.log("Loading Data...");
    var num = 0;
    var data = Load(saveName);
    listItemsInput.forEach(function(x)
    {
        x.checked = (data[num] === 'true');
        num += 1;
    })
    countItems();
    console.log(data);
}

function countItems()
{
    var num = 0;
    listItemsInput.forEach(function(item)
    {
        if (item.checked)
        {
            num += 1;
        }
    })
    count.innerHTML = "Count: " + num + "/" + (listItemsInput.length);
}

function filter()
{
    var search = input.value.toLowerCase();

    listItems.forEach(function(dt)
    {
        var text = dt.innerHTML.toLowerCase();
        var found = text.indexOf(search);
        console.log(dt);
        if (search == '')
        {
            dt.style.display = 'block';
        } else if (found == -1)
        {
            dt.style.display = 'none';
        } else
        {
            dt.style.display = 'block';
        }
    })

}