export function Save(name, data)
{
    let temp = data.split("|");
    temp.pop();
    data = temp.join();
    localStorage.setItem(name, data);
    console.log(data);
}

export function Load(name)
{
    console.log("Loading Data...");
    var num = 0;
    if (localStorage.getItem(name) == null || localStorage.getItem(name) == "")
    {
        console.log("No Data");
        return;
    }
    var data = localStorage.getItem(name).split(",");
    console.log(data);
    return data;  
}

export function Complete(name)
{
    console.log("Checking Completion...")
    if (localStorage.getItem(name) == null || localStorage.getItem(name) == "")
    {
        console.log("No Data");
        return;
    }
    var data = localStorage.getItem(name).split(",");
    for (var i =0; i<data.length;i++)
    {
        if (data[i] == "false")
            return false;
    }
    return true;
}